let BACKEND_URL;
if (process.env.NODE_ENV === "production") {
  BACKEND_URL = "https://no-asthma.ml/api";
} else {
  BACKEND_URL = "https://no-asthma.ml/api";
}
let _settings = {
  ENDPOINTS: {
    SIGN_IN_URL: `${BACKEND_URL}/sign_in/`,
    SIGN_UP_URL: `${BACKEND_URL}/sign_up/`,
    USER_DETAILS: `${BACKEND_URL}/user_details/`,
    MEDICATIONS: `${BACKEND_URL}/medications/`,
    QUIZ: `${BACKEND_URL}/quiz/`,
    EVENTS: `${BACKEND_URL}/events/`
  }
};
window.settings = _settings;
export const settings = _settings;
