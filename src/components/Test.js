import React from "react";
import { Form, Field } from "react-final-form";
import Button from "@material-ui/core/Button";
import { localizedText } from "./Localization";
import { postWithAuth } from "../api";

async function postQuiz(values) {
  try {
    console.log(values);
    const dataToPost = { questions: [] };
    for (let key in values) {
      let obj = { name: key, answer: Number(values[key]) };
      dataToPost.questions.push(obj);
    }
    console.log(dataToPost);
    const response = await postWithAuth(
      window.settings.ENDPOINTS.QUIZ,
      dataToPost
    );
    return response.data;
  } catch (e) {
    console.log(e);
  }
}

export class Test extends React.Component {
  render() {
    const { language } = this.props;
    return (
      <Form
        onSubmit={postQuiz}
        render={({ handleSubmit }) => (
          <div>
            <form onSubmit={handleSubmit}>
              <div className="form-check">
                <h3>{localizedText[language].tesTask1}</h3>
                <label>
                  <Field
                    name="workload"
                    render={({ input, meta }) => (
                      <input
                        {...input}
                        type="radio"
                        value={3}
                        className="form-check-input"
                      />
                    )}
                  />
                  {localizedText[language].testAnsw11}
                </label>
              </div>

              <div className="form-check">
                <label>
                  <Field
                    name="workload"
                    render={({ input, meta }) => (
                      <input
                        {...input}
                        type="radio"
                        value={2}
                        className="form-check-input"
                      />
                    )}
                  />
                  {localizedText[language].testAnsw12}
                </label>
              </div>

              <div className="form-check">
                <label>
                  <Field
                    name="workload"
                    render={({ input, meta }) => (
                      <input
                        {...input}
                        type="radio"
                        value={0}
                        className="form-check-input"
                      />
                    )}
                  />
                  {localizedText[language].testAnsw13}
                </label>
              </div>
              <div className="form-check">
                <label>
                  <h3>{localizedText[language].tesTask2}</h3>
                  <Field
                    name="breathing"
                    render={({ input, meta }) => (
                      <input
                        {...input}
                        type="radio"
                        value={3}
                        className="form-check-input"
                      />
                    )}
                  />
                  {localizedText[language].testAnsw21}
                </label>
              </div>

              <div className="form-check">
                <label>
                  <Field
                    name="breathing"
                    render={({ input, meta }) => (
                      <input
                        {...input}
                        type="radio"
                        value={2}
                        className="form-check-input"
                      />
                    )}
                  />
                  {localizedText[language].testAnsw22}
                </label>
              </div>

              <div className="form-check">
                <label>
                  <Field
                    name="breathing"
                    render={({ input, meta }) => (
                      <input
                        {...input}
                        type="radio"
                        value={0}
                        className="form-check-input"
                      />
                    )}
                  />
                  {localizedText[language].testAnsw23}
                </label>
              </div>
              <div className="form-check">
                <label>
                  <h3>{localizedText[language].tesTask3}</h3>
                  <Field
                    name="wakeup"
                    render={({ input, meta }) => (
                      <input
                        {...input}
                        type="radio"
                        value={3}
                        className="form-check-input"
                      />
                    )}
                  />
                  {localizedText[language].testAnsw31}
                </label>
              </div>

              <div className="form-check">
                <label>
                  <Field
                    name="wakeup"
                    render={({ input, meta }) => (
                      <input
                        type="radio"
                        {...input}
                        value={2}
                        className="form-check-input"
                      />
                    )}
                  />
                  {localizedText[language].testAnsw32}
                </label>
              </div>

              <div className="form-check">
                <label>
                  <Field
                    name="wakeup"
                    render={({ input, meta }) => (
                      <input
                        {...input}
                        type="radio"
                        value={1}
                        className="form-check-input"
                      />
                    )}
                  />
                  {localizedText[language].testAnsw33}
                </label>
              </div>
              <div className="form-check">
                <label>
                  <h3>{localizedText[language].tesTask4}</h3>
                  <Field
                    name="medications"
                    render={({ input, meta }) => (
                      <input
                        {...input}
                        type="radio"
                        value={3}
                        className="form-check-input"
                      />
                    )}
                  />
                  {localizedText[language].testAnsw41}
                </label>
              </div>

              <div className="form-check">
                <label>
                  <Field
                    name="medications"
                    render={({ input, meta }) => (
                      <input
                        {...input}
                        type="radio"
                        value={2}
                        className="form-check-input"
                      />
                    )}
                  />
                  {localizedText[language].testAnsw42}
                </label>
              </div>

              <div className="form-check">
                <label>
                  <Field
                    name="medications"
                    render={({ input, meta }) => (
                      <input
                        {...input}
                        type="radio"
                        value={1}
                        className="form-check-input"
                      />
                    )}
                  />
                  {localizedText[language].testAnsw43}
                </label>
              </div>
              <div className="form-check">
                <label>
                  <h3>{localizedText[language].tesTask5}</h3>
                  <Field
                    name="management"
                    render={({ input, meta }) => (
                      <input
                        {...input}
                        type="radio"
                        value={3}
                        className="form-check-input"
                      />
                    )}
                  />
                  {localizedText[language].testAnsw51}
                </label>
              </div>

              <div className="form-check">
                <label>
                  <Field
                    name="management"
                    render={({ input, meta }) => (
                      <input
                        {...input}
                        type="radio"
                        value={2}
                        className="form-check-input"
                      />
                    )}
                  />
                  {localizedText[language].testAnsw52}
                </label>
              </div>

              <div className="form-check">
                <label>
                  <Field
                    name="management"
                    render={({ input, meta }) => (
                      <input
                        {...input}
                        type="radio"
                        value={1}
                        className="form-check-input"
                      />
                    )}
                  />
                  {localizedText[language].testAnsw53}
                </label>
              </div>
              <div className="form-group">
                <Button
                  variant="outlined"
                  color="primary"
                  type="submit"
                  className="btn btn-primary mt-2"
                >
                  Save
                </Button>
              </div>
            </form>
          </div>
        )}
      />
    );
  }
}
