import React from "react";
import styled from "styled-components";
import { Grid } from "@material-ui/core";
import symptoms from "../images/symptoms.png";
import { localizedText } from "./Localization";

const Text = styled.p`
  text-align: justify;
`;

export function Symptoms({ language }) {
  return (
    <div>
      {localizedText[language].symptomsTitle}
      <Grid container spasing={3}>
        <Grid item xs={1}></Grid>
        <Grid item xs={3}>
          <img alt={"img"} src={symptoms} style={{ width: "100%" }} />
        </Grid>
        <Grid item xs={1}></Grid>
        <Grid item xs={5}>
          <Text>{localizedText[language].symptomsText}</Text>
        </Grid>
      </Grid>
    </div>
  );
}
