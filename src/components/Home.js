import React from "react";
import { Grid } from "@material-ui/core";
import styled from "styled-components";
import first from "../images/first.png";
import { makeStyles } from "@material-ui/core/styles";
import { localizedText } from "./Localization";
import Button from "@material-ui/core/Button";
import { isAuthenticated } from "../api";

const Text = styled.p`
  text-align: justify;
`;
const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1)
  },
  input: {
    display: "none"
  }
}));

export function Home({
  currentTabSetter,
  signInIndex,
  signUpIndex,
  language,
  setLanguage
}) {
  const classes = useStyles();
  return (
    <div>
      {localizedText[language].homeTitle}
      <Grid container spasing={3}>
        <Grid item xs={1}></Grid>
        <Grid item xs={5}>
          <Text>{localizedText[language].homeText}</Text>
          {!isAuthenticated() && (
            <Button
              variant="outlined"
              color="primary"
              className={classes.button}
              onClick={() => currentTabSetter(signInIndex)}
            >
              Sign In
            </Button>
          )}
          {!isAuthenticated() && (
            <Button
              variant="outlined"
              color="primary"
              className={classes.button}
              onClick={() => currentTabSetter(signUpIndex)}
            >
              Sign Up
            </Button>
          )}
        </Grid>

        <Grid item xs={1}></Grid>
        <Grid item xs={3}>
          <img alt={"img"} src={first} style={{ width: "100%" }} />
        </Grid>
        <Grid item xs={1}>
          <label>
            <input
              type="radio"
              name="language"
              checked={language === "en"}
              onChange={() => setLanguage("en")}
            />
            En
          </label>
          <label>
            <input
              type="radio"
              name="language"
              checked={language === "uk"}
              onChange={() => setLanguage("uk")}
            />
            Uk
          </label>
        </Grid>
      </Grid>
    </div>
  );
}
