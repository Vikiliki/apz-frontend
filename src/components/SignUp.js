import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { isAuthenticated, signUp } from "../api";
import { Field, Form } from "react-final-form";

const useStyles = makeStyles(theme => ({
  "@global": {
    body: {
      backgroundColor: theme.palette.common.white
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

export function SignUp() {
  const classes = useStyles();
  if (isAuthenticated()) {
    return <div />;
  }

  return (
    <Form
      onSubmit={signUp}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
              <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Sign up
              </Typography>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <Field
                    name="first_name"
                    render={({ input, meta }) => (
                      <TextField
                        {...input}
                        autoComplete="fname"
                        variant="outlined"
                        required
                        fullWidth
                        label="First Name"
                        autoFocus
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <Field
                    name="last_name"
                    render={({ input, meta }) => (
                      <TextField
                        {...input}
                        variant="outlined"
                        required
                        fullWidth
                        label="Last Name"
                        autoComplete="lname"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Field
                    name="email"
                    render={({ input, meta }) => (
                      <TextField
                        {...input}
                        variant="outlined"
                        required
                        fullWidth
                        label="Email Address"
                        autoComplete="email"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Field
                    name="password"
                    render={({ input, meta }) => (
                      <TextField
                        {...input}
                        variant="outlined"
                        required
                        fullWidth
                        label="Password"
                        type="password"
                        autoComplete="current-password"
                      />
                    )}
                  />
                </Grid>
              </Grid>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                Sign Up
              </Button>
            </div>
          </Container>
        </form>
      )}
    />
  );
}
