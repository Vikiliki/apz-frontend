import React, { useState, useEffect } from "react";
import { Form, Field } from "react-final-form";
import { Grid } from "@material-ui/core";
import { TextField } from "@material-ui/core";
import styled from "styled-components";
import first from "../images/first.png";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { localizedText } from "./Localization";
import { Calendar } from "./Calendar";
import {
  isAuthenticated,
  getWithAuth,
  patchWithAuth,
  postWithAuth
} from "../api";

const Text = styled.p`
  text-align: left;
`;
const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1)
  },
  input: {
    display: "none"
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 200
  }
}));

async function getUserDetails(setUserDetailsData) {
  try {
    const response = await getWithAuth(window.settings.ENDPOINTS.USER_DETAILS);
    setUserDetailsData(response.data);
  } catch (e) {
    console.log(e);
  }
}

async function updateUserDetails(values) {
  try {
    const response = await patchWithAuth(
      window.settings.ENDPOINTS.USER_DETAILS,
      values
    );
    return response;
  } catch (e) {
    console.log(e);
  }
}

async function getMedications(setMedications) {
  try {
    const response = await getWithAuth(window.settings.ENDPOINTS.MEDICATIONS);
    if (response.data.medications.length > 0) {
      setMedications(response.data.medications[0]);
    }
  } catch (e) {
    console.log(e);
  }
}

async function updateOrCreateMedications(values) {
  try {
    if (values.id) {
      const response = await patchWithAuth(
        window.settings.ENDPOINTS.MEDICATIONS,
        values
      );
      return response;
    } else {
      const response = await postWithAuth(
        window.settings.ENDPOINTS.MEDICATIONS,
        values
      );
      return response;
    }
  } catch (e) {
    console.log(e);
  }
}

async function getLastQuizResult(setQuiz) {
  try {
    const response = await getWithAuth(window.settings.ENDPOINTS.QUIZ);
    if (!response.data) {
      setQuiz("Pass the test");
    } else {
      let result = 0;
      for (let obj of response.data.questions) {
        result += obj.answer;
      }
      setQuiz(String(result));
    }
  } catch (e) {
    console.log(e);
  }
}

export function Personal({ currentTabSetter, testPageIndex, language }) {
  const classes = useStyles();
  const [userDetailsData, setUserDetailsData] = useState({
    first_name: "",
    last_name: ""
  });
  const [medicationsData, setMedications] = useState({
    medication: "",
    dosage: ""
  });
  const [quizResult, setQuizResult] = useState("Pass the test");
  useEffect(() => {
    getUserDetails(setUserDetailsData);
    getMedications(setMedications);
    getLastQuizResult(setQuizResult);
  }, []);
  if (!isAuthenticated()) {
    return <div />;
  }
  return (
    <div>
      <h3>{localizedText[language].personalMD}</h3>
      <hr></hr>
      <Grid container spasing={3}>
        <Grid item xs={2}></Grid>
        <Grid item xs={2}>
          <img alt={"img"} src={first} style={{ width: "85%" }} />
        </Grid>
        <Grid item xs={2}></Grid>
        <Form
          onSubmit={updateUserDetails}
          initialValues={userDetailsData}
          render={({ handleSubmit }) => (
            <form onSubmit={handleSubmit}>
              <div>
                <Field
                  name="first_name"
                  render={({ input, meta }) => (
                    <TextField {...input} label="First name" margin="normal" />
                  )}
                />
              </div>
              <div>
                <Field
                  name="last_name"
                  render={({ input, meta }) => (
                    <TextField {...input} label="Last name" margin="normal" />
                  )}
                />
              </div>
              <div>
                <Button
                  type="submit"
                  variant="outlined"
                  color="primary"
                  className={classes.button}
                >
                  Save
                </Button>
              </div>
            </form>
          )}
        />
      </Grid>
      <hr></hr>
      <h3>{localizedText[language].personalMM}</h3>
      <hr></hr>
      <Form
        onSubmit={updateOrCreateMedications}
        validate={() => {}}
        initialValues={medicationsData}
        render={({ handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <Grid container spasing={3}>
              <Grid item xs={2}></Grid>
              <Grid item xs={2}>
                <Field
                  name="medication"
                  render={({ input, meta }) => (
                    <TextField {...input} label="Medication" margin="normal" />
                  )}
                />
              </Grid>
              <Grid item xs={1}></Grid>
              <Grid item xs={2}>
                <Field
                  name="dosage"
                  render={({ input, meta }) => (
                    <TextField {...input} label="Dosage" margin="normal" />
                  )}
                />
              </Grid>
              <Grid item xs={1}></Grid>
              <Button
                type="submit"
                variant="outlined"
                color="primary"
                className={classes.button}
              >
                Add a medication
              </Button>
            </Grid>
          </form>
        )}
      />
      <hr></hr>
      <h3>{localizedText[language].personalS}</h3>
      <hr></hr>
      <p>{localizedText[language].personalSText1}</p>
      <Grid container spasing={3} style={{ minHeight: 300 }}>
        <Grid item xs={2}></Grid>
        <Grid item xs={4}>
          <Calendar />
        </Grid>
        <Grid item xs={1}></Grid>
        <Grid item xs={4}>
          <Text>{localizedText[language].personalSText}</Text>
        </Grid>
      </Grid>
      <hr></hr>
      <h3>{localizedText[language].personalT}</h3>
      <hr></hr>
      <Grid container spasing={3}>
        <Grid item xs={2}></Grid>
        <Grid item xs={2}>
          <h3>Your test result is: {quizResult} </h3>
        </Grid>
        <Grid item xs={4}></Grid>
        <Button
          input
          type="submit"
          value="Add"
          variant="outlined"
          color="primary"
          className={classes.button}
          onClick={() => currentTabSetter(testPageIndex)}
        >
          Pass the test
        </Button>
      </Grid>
      <hr></hr>
      {/*<h3>{localizedText[language].personalG}</h3>
      <hr></hr>*/}
      {/*<SignUp/>
      <SignIn/>*/}
    </div>
  );
}
