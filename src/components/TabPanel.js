import React from "react";
import styled from "styled-components";

const Wrapper = styled.div``;

export function TabPanel({ index, value, children }) {
  return <Wrapper hidden={value !== index}>{children}</Wrapper>;
}
