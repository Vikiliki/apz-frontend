import React from "react";
import styled from "styled-components";
import { Grid } from "@material-ui/core";
import triggers1 from "../images/triggers1.png";
import triggers2 from "../images/triggers2.png";
import { localizedText } from "./Localization";

const Text = styled.p`
  text-align: justify;
`;

export function AsthmaTriggers({ language }) {
  return (
    <div>
      {localizedText[language].triggersTitle}
      <Grid container spasing={3}>
        <Grid item xs={1}></Grid>
        <Grid item xs={5}>
          <Text>{localizedText[language].triggersText}</Text>
        </Grid>
        <Grid item xs={1}></Grid>
        <Grid item xs={3}>
          <img alt={"img"} src={triggers1} style={{ width: "100%" }} />
        </Grid>
      </Grid>
      {localizedText[language].reduceTitle}
      <Grid container spasing={3}>
        <Grid item xs={1}></Grid>
        <Grid item xs={3}>
          <img alt={"img"} src={triggers2} style={{ width: "50%" }} />
        </Grid>
        <Grid item xs={1}></Grid>
        <Grid item xs={5}>
          <Text>{localizedText[language].reduceText}</Text>
        </Grid>
      </Grid>
    </div>
  );
}
