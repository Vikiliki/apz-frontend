import React from "react";
import styled from "styled-components";
import { Grid } from "@material-ui/core";
import reduce from "../images/reduce.png";
import { localizedText } from "./Localization";

const Text = styled.p`
  text-align: justify;
`;

export function Prevention({ language }) {
  return (
    <div>
      {localizedText[language].preventionTitle}
      <Grid container spasing={3}>
        <Grid item xs={1}></Grid>
        <Grid item xs={3}>
          <img src={reduce} style={{ width: "120%" }} alt={"img"} />
        </Grid>
        <Grid item xs={1}></Grid>
        <Grid item xs={5}>
          <Text>{localizedText[language].preventionText}</Text>
        </Grid>
      </Grid>
    </div>
  );
}
