import React from "react";

export const localizedText = {
  en: {
    homeTitle: <h1>Overview</h1>,
    homeText: (
      <div>
        <p>
          Asthma is a condition in which your airways narrow and swell and
          produce extra mucus. This can make breathing difficult and trigger
          coughing, wheezing and shortness of breath. For some people, asthma is
          a minor nuisance.
        </p>
        <p>
          For others, it can be a major problem that interferes with daily
          activities and may lead to a life-threatening asthma attack.
        </p>
        <p>
          Asthma can't be cured, but its symptoms can be controlled. Because
          asthma often changes over time, it's important that you work with your
          doctor to track your signs and symptoms and adjust treatment as
          needed.
        </p>
        <p>
          Modern mobile technologies are able to facilitate the daily routine of
          patients. With the help of special applications, asthmatics can
          control their condition and monitor their well-being. And one of them
          is the NoAsthma app.
        </p>
      </div>
    ),
    whatAsthmaTitle: <h1>What is asthma</h1>,
    whatAsthmaText: (
      <div>
        <p>
          Asthma is an incurable illness of the airways. The disease causes
          inflammation and narrowing inside the lung, restricting air supply.
        </p>
        <p>
          The symptoms of asthma often present in periodic attacks or episodes
          of tightness in the chest, wheezing, breathlessness, and coughing.
        </p>
        <p>
          During the development of asthma, the airways swell and become
          extremely sensitive to some of the substances a person might inhale.
        </p>
        <p>
          When this increased sensitivity causes a reaction, the muscles that
          control the airways tighten. In doing so, they might restrict the
          airways even further and trigger an overproduction of mucus.
        </p>
      </div>
    ),
    typeTitle: <h1>Types</h1>,
    typeText: (
      <div>
        <p>
          As many different factors come together to cause asthma, there are
          many different types of the disease, separated by age and severity.
          Adults and children share the same triggers for symptoms that set off
          an allergic response in the airways, including airborne pollutants,
          mold, mildew, and cigarette smoke.
        </p>
        <ul>
          <li>
            Childhood asthma: Children are more likely to have an intermittent
            form of asthma that presents in severe attacks. Some children might
            experience daily symptoms, but the common characteristic among
            children with asthma is a heightened sensitivity to substances that
            cause allergy.
          </li>
          <li>
            Adult-onset asthma: Asthma in adults is often persistent and
            requires the daily management of flare-ups and preventing symptoms.
            Asthma can begin at any age.
          </li>
          <li>
            Occupational asthma: This is a type of asthma that occurs as a
            direct result of a job or profession.
          </li>
          <li>
            Seasonal asthma: This type occurs in response to allergens that are
            only in the surrounding environment at certain times of year, such
            as cold air in the winter or pollen during hay fever season.
          </li>
        </ul>
      </div>
    ),
    symptomsTitle: <h1>Symptoms</h1>,
    symptomsText: (
      <div>
        <p>
          Asthma symptoms vary from person to person. You may have infrequent
          asthma attacks, have symptoms only at certain times — such as when
          exercising — or have symptoms all the time.
        </p>
        <p>Asthma signs and symptoms include:</p>
        <ul>
          <li>Shortness of breath</li>
          <li>Chest tightness or pain</li>
          <li>
            Trouble sleeping caused by shortness of breath, coughing or wheezing
          </li>
          <li>
            A whistling or wheezing sound when exhaling (wheezing is a common
            sign of asthma in children)
          </li>
          <li>
            Coughing or wheezing attacks that are worsened by a respiratory
            virus, such as a cold or the flu
          </li>
        </ul>
      </div>
    ),
    triggersTitle: <h1>Asthma triggers</h1>,
    triggersText: (
      <div>
        <p>
          Exposure to various irritants and substances that trigger allergies
          (allergens) can trigger signs and symptoms of asthma. Asthma triggers
          are different from person to person and can include:
        </p>
        <ul>
          <li>
            Airborne substances, such as pollen, dust mites, mold spores, pet
            dander or particles of cockroach waste
          </li>
          <li>Respiratory infections, such as the common cold</li>
          <li>Physical activity (exercise-induced asthma)</li>
          <li>Cold air</li>
          <li>Air pollutants and irritants, such as smoke</li>
          <li>Strong emotions and stress</li>
          <li>
            Sulfites and preservatives added to some types of foods and
            beverages, including shrimp, dried fruit, processed potatoes, beer
            and wine
          </li>
        </ul>
      </div>
    ),
    reduceTitle: <h1>Steps to reduce your triggers</h1>,
    reduceText: (
      <div>
        <ul>
          <li>Use your air conditioner.</li>
          <li>Decontaminate your decor, clean regularly.</li>
          <li>Maintain optimal humidity.</li>
          <li>Prevent mold spores.</li>
          <li>Reduce pet dander.</li>
          <li>Cover your nose and mouth if it's cold out.</li>
        </ul>
      </div>
    ),
    preventionTitle: <h1>Prevention</h1>,
    preventionText: (
      <div>
        <p>
          While there's no way to prevent asthma, by working together, you and
          your doctor can design a step-by-step plan for living with your
          condition and preventing asthma attacks.
        </p>
        <ul>
          <li>
            Follow your asthma action plan. With your doctor and health care
            team, write a detailed plan for taking medications and managing an
            asthma attack. Then be sure to follow your plan.
          </li>
          <li>
            Get vaccinated for influenza and pneumonia. Staying current with
            vaccinations can prevent flu and pneumonia from triggering asthma
            flare-ups.
          </li>
          <li>
            Identify and avoid asthma triggers. A number of outdoor allergens
            and irritants — ranging from pollen and mold to cold air and air
            pollution — can trigger asthma attacks. Find out what causes or
            worsens your asthma, and take steps to avoid those triggers.
          </li>
          <li>
            Monitor your breathing. You may learn to recognize warning signs of
            an impending attack, such as slight coughing, wheezing or shortness
            of breath. But because your lung function may decrease before you
            notice any signs or symptoms, regularly measure and record your peak
            airflow with a home peak flow meter.
          </li>
          <li>
            Take your medication as prescribed. Just because your asthma seems
            to be improving, don't change anything without first talking to your
            doctor. It's a good idea to bring your medications with you to each
            doctor visit, so your doctor can double-check that you're using your
            medications correctly and taking the right dose.
          </li>
          <li>
            Pay attention to increasing quick-relief inhaler use. If you find
            yourself relying on your quick-relief inhaler, such as albuterol,
            your asthma isn't under control. See your doctor about adjusting
            your treatment.
          </li>
        </ul>
      </div>
    ),
    breathTitle: <h1>Breathing Exercises</h1>,
    breathText: (
      <div>
        <p>
          Experts now believe that asthmatics tend to breath faster than people
          with normal lungs, and many also have a tendency to be mouth
          breathers.
        </p>
        <ul>
          <li>
            Diaphragmatic breathing: This is a basic and simple breathing
            technique that maximizes air distribution in your lungs.
          </li>
          <li>
            Buteyko breathing: This is a breathing technique that teaches
            asthmatics to consciously reduce either breathing rate or breathing
            volume.
          </li>
          <li>
            Physical movement exercises: This type of breathing exercise
            combines physical elements and breathing elements. Focus on good
            posture.
          </li>
          <li>
            Yoga: Studies that regular yoga participation reduced asthma
            symptoms and rescue inhaler use by 43 percent. In doing yoga you
            hold poses and concentrate on your breathing.
          </li>
          <li>
            Papworth method: This type is similar to diaphragmatic breathing and
            Buteyko method. These breathing exercises are believed to be
            beneficial to patients with mild asthma.{" "}
          </li>
          <li>
            Pursed lip breathing: This can be used when you are having an asthma
            attack. Since asthma causes air to become trapped in your lungs,
            this may help you get more air out and may make breathing easier.
          </li>
          <li>
            Progressive relaxation technique: This technique helps to relax all
            the muscles in your body. Lie down and close your eyes. Concentrate
            on breathing through your nose.{" "}
          </li>
        </ul>
      </div>
    ),
    personalMD: "My Details",
    personalMM: "My Medications",
    personalS: "Symptoms",
    personalSText1: "To add symptoms, select the desired day and write them.",
    personalSText: (
      <div>
        <ul style={{ color: "green" }}>
          <li>No cough, wheeze, chest tightness during the day or night</li>
          <li>Can do usual activities</li>
        </ul>
        <ul style={{ color: "orange" }}>
          <li>Cough</li>
          <li>Wheeze</li>
          <li>Chest tightness</li>
          <li>Shortness of breath</li>
          <li>Waking at night due to asthma</li>
          <li>Can do some, but not all, usual activities</li>
        </ul>
        <ul style={{ color: "red" }}>
          <li>Very short of breath</li>
          <li>Quick-relief medicines have not helped</li>
          <li>Cannot do usual activities</li>
        </ul>
      </div>
    ),
    personalT: "Test",
    personalG: "Graph",
    tesTask1:
      "How often over the past week asthma has prevented you from doing your usual workload?",
    tesTask2:
      "How often in the last week have you noticed difficulty breathing?",
    tesTask3:
      "How often in the last week you woke up at night or earlier than usual due to asthma symptoms?",
    tesTask4: "How often have you used medications in the last week?",
    tesTask5:
      "How would you rate how you managed to control your asthma symptoms in the past week?",
    testAnsw11: "All time or very often",
    testAnsw12: "Sometimes",
    testAnsw13: "Rarely or never",
    testAnsw21: "Once per day or more",
    testAnsw22: "3-6 times per week",
    testAnsw23: "1-2 times per week or never",
    testAnsw31: "3-4 nights per week or more",
    testAnsw32: "1-2 nights per week",
    testAnsw33: "1 night per week or never",
    testAnsw41: "3 times per week or more often",
    testAnsw42: "1-2 times per week",
    testAnsw43: "1 time per week or never",
    testAnsw51: "Poorly managed",
    testAnsw52: "Managed to some extent",
    testAnsw53: "Well managed"
  },
  uk: {
    homeTitle: <h1>Загальне уявлення</h1>,
    homeText: (
      <div>
        <p>
          Астма - це стан, при якому ваші дихальні шляхи звужуються, набрякають
          і виробляють зайвий слиз. Це може ускладнити дихання і спровокувати
          кашель, хрипи та задишку. Для деяких людей астма - незначна
          неприємність.
        </p>
        <p>
          Для інших це може бути основною проблемою, яка заважає щоденній
          діяльності і може призвести до небезпечного для життя нападу астми.
        </p>
        <p>
          Астму неможливо вилікувати, але її симптоми можна контролювати.
          Оскільки астма часто змінюється з часом, важливо, щоб ви працювали зі
          своїм лікарем, щоб відстежувати ваші ознаки та симптоми та коригувати
          лікування за потребою.
        </p>
        <p>
          Сучасні мобільні технології здатні полегшити розпорядок дня пацієнтів.
          За допомогою спеціальних застосунків астматики можуть контролювати
          свій стан і стежити за своїм самопочуттям. І одним з них є додаток
          NoAsthma.
        </p>
      </div>
    ),
    whatAsthmaTitle: <h1>Що таке астма</h1>,
    whatAsthmaText: (
      <div>
        <p>
          Астма - невиліковна хвороба дихальних шляхів. Захворювання викликає
          запалення і звуження всередині легенів, обмежуючи подачу повітря.
        </p>
        <p>
          Симптоми астми часто присутні в періодичних нападах або епізодах
          напруги в грудях, хрипах, задухи та кашлю.
        </p>
        <p>
          Під час розвитку астми дихальні шляхи набрякають і стають надзвичайно
          чутливими до деяких речовин, які людина може вдихати.
        </p>
        <p>
          Коли ця підвищена чутливість викликає реакцію, м’язи, які контролюють
          дихальні шляхи, напружуються. При цьому вони можуть ще більше обмежити
          дихальні шляхи і викликати надвиробництво слизу.
        </p>
      </div>
    ),
    typeTitle: <h1>Типи</h1>,
    typeText: (
      <div>
        <p>
          Оскільки астму викликають багато різних факторів, одночасно поєднаних,
          існує декілька різних типів захворювання, розділених за віком та
          тяжкістю. Дорослі та діти поділяють однакові тригери щодо симптомів,
          що спричиняють алергічну реакцію в дихальних шляхах, включаючи
          забруднювачі повітря, цвіль і сигаретний дим.
        </p>
        <ul>
          <li>
            {" "}
            Дитяча астма: у дітей швидше спостерігається переривчаста форма
            астми, яка проявляється у важких нападах. У деяких дітей можуть
            виникати щоденні симптоми, але загальною характеристикою серед дітей
            з астмою є підвищена чутливість до речовин, що викликають алергію.
          </li>
          <li>
            Астма у дорослому віці: астма у дорослих часто є стійкою і вимагає
            щоденного управління спалахами та запобігання симптомів. Астма може
            початися в будь-якому віці.
          </li>
          <li>
            Професійна астма: Це тип астми, що виникає як безпосередній
            результат роботи або професії.
          </li>
          <li>
            Сезонна астма: Цей тип виникає у відповідь на алергени, які є лише в
            навколишньому середовищі в певний час року, наприклад, холодне
            повітря взимку або пилок під час сезону сінної лихоманки.
          </li>
        </ul>
      </div>
    ),
    symptomsTitle: <h1>Симптоми</h1>,
    symptomsText: (
      <div>
        <p>
          Симптоми астми відрізняються від людини до людини. Ви можете мати
          нечасті напади астми, мати симптоми лише в певний час - наприклад, під
          час фізичних вправ - або симптоми, що проявляються постійно.
        </p>
        <p>До ознак та симптомів астми належать:</p>
        <ul>
          <li>Задишка.</li>
          <li>Стиснення грудей або біль. </li>
          <li>Смутний сон, спричинений задишкою, кашлем або хрипами. </li>
          <li>
            Свистячий чи хриплий звук при видиху (хрипи - це звичайна ознака
            астми у дітей).{" "}
          </li>
          <li>
            Напади кашлю або хрипів, які погіршуються респіраторним вірусом,
            наприклад, застудою або грипом
          </li>
        </ul>
      </div>
    ),
    triggersTitle: <h1>Збудники астми</h1>,
    triggersText: (
      <div>
        <p>
          Вплив різних подразників та речовин, що викликають алергію (алергени),
          може спровокувати ознаки та симптоми астми. Тригери астми
          відрізняються від людини до людини і можуть включати:
        </p>
        <ul>
          <li>
            Повітряні речовини, такі як пилок, пилові кліщі, спори цвілі, лупа
            домашніх тварин або частинки відходів таргана.
          </li>
          <li>Дихальні інфекції, такі як звичайна застуда.</li>
          <li>Фізичні навантаження (астма, спричинена фізичними вправами).</li>
          <li>Холодне повітря.</li>
          <li>Забруднювачі повітря та подразники, такі як дим.</li>
          <li>Сильні емоції та стрес.</li>
          <li>
            Сульфіти та консерванти, що додаються до деяких видів продуктів
            харчування та напоїв, включаючи креветки, сухофрукти, оброблену
            картоплю, пиво та вино
          </li>
        </ul>
      </div>
    ),
    reduceTitle: <h1>Шляхи зменшення збудників астми</h1>,
    reduceText: (
      <div>
        <ul>
          <li>Використовуйте свій кондиціонер.</li>
          <li>Обеззаражуйте декор, регулярно робіть прибирання.</li>
          <li>Підтримуйте оптимальну вологість.</li>
          <li>Запобігайте виникненню спор цвілі.</li>
          <li>Зменшіть вплив домашніх тварин.</li>
          <li>Прикривайте ніс і рот, якщо холодно.</li>
        </ul>
      </div>
    ),
    preventionTitle: <h1>Профілактика</h1>,
    preventionText: (
      <div>
        <p>
          Хоча немає можливості запобігти виникненню астми, працюючи разом, ви
          та ваш лікар можете розробити покроковий план життя за вашим станом та
          запобігання нападів астми.
        </p>
        <ul>
          <li>
            Дотримуйтесь свого плану дій при астмі. Разом зі своїм лікарем та
            командою охорони здоров’я складіть детальний план прийому ліків та
            лікування нападів астми. Обов’язково дотримуйтесь свого плану.
          </li>
          <li>
            Зробіть вакцинацію проти грипу та пневмонії. Залишаючись здоровим за
            допомогою вакцинації, можна запобігти виникненню спалахів грипу та
            пневмонії.
          </li>
          <li>
            Виявити та уникнути тригерів астми. Ряд зовнішніх алергенів та
            подразників - від пилку та цвілі до холодного та забрудненного
            повітря - можуть спровокувати напади астми. Дізнайтеся, що викликає
            або погіршує вашу астму, і вживайте заходів, щоб уникнути цих
            тригерів.
          </li>
          <li>
            Контролюйте своє дихання. Ви можете навчитися розпізнавати
            попереджувальні ознаки насування нападу, такі як легкий кашель,
            хрипи або задишка. Але оскільки ваша функція легенів може
            зменшитися, перш ніж ви помітите будь-які ознаки чи симптоми,
            регулярно вимірюйте та записуйте свій піковий потік повітря домашнім
            піковим вимірювачем.
          </li>
          <li>
            Приймайте ліки за призначенням. Тільки тому, що ваша астма, схоже,
            покращується, нічого не змінюйте, попередньо не поговоривши з
            лікарем. Це хороша ідея носити свої препарати з собою на кожен візит
            до лікаря, щоб ваш лікар міг повторно перевірити, чи правильно ви
            вживаєте ліки та приймаєте потрібну дозу.
          </li>
          <li>
            Зверніть увагу на збільшення швидкого використання інгаляторів. Якщо
            ви покладаєтесь на свій інгалятор швидкого полегшення, наприклад,
            альбутерол, ваша астма не контролюється. Зверніться до лікаря щодо
            коригування вашого лікування.
          </li>
        </ul>
      </div>
    ),
    breathTitle: <h1>Дихальні вправи</h1>,
    breathText: (
      <div>
        <p>
          Зараз фахівці вважають, що астматики, як правило, дихають швидше, ніж
          люди з нормальними легенями, і багато людей також мають тенденцію
          дихати через рот.
        </p>
        <ul>
          <li>
            Діафрагмальне дихання: це основний і простий прийом дихання, який
            максимально розподіляє повітря в легенях.{" "}
          </li>
          <li>
            Дихання Бутейко: Це дихальна техніка, яка вчить астматиків свідомо
            знижувати частоту дихання або об'єм дихання.{" "}
          </li>
          <li>
            Фізичні вправи для руху: Цей тип дихальної вправи поєднує фізичні
            елементи та елементи дихання. Орієнтуйтеся на хорошу поставу.{" "}
          </li>
          <li>
            Йога: Дослідженно, що регулярні заняття йогою зменшують симптоми
            астми та використання рятувальних інгаляторів на 43 відсотки.
            Займаючись йогою, ви тримаєте пози і концентруєтесь на своєму
            диханні.{" "}
          </li>
          <li>
            Метод Папворта: Цей тип схожий на діафрагмальне дихання та метод
            Бутейко. Вважається, що ці дихальні вправи корисні для пацієнтів із
            легкою астмою.{" "}
          </li>
          <li>
            Задимлене дихання губами: Це можна використовувати, коли у вас напад
            астми. Оскільки астма перешкоджує потрапляння повітря в легені, це
            може допомогти вам отримати більше повітря і полегшити дихання.{" "}
          </li>
          <li>
            Прогресивна техніка розслаблення: Ця методика допомагає розслабити
            всі м’язи вашого тіла. Ляжте і закрийте очі. Концентруйтеся на
            диханні через ніс.
          </li>
        </ul>
      </div>
    ),
    personalMD: "Інформація про мене",
    personalMM: "Мої ліки",
    personalS: "Симптоми",
    personalSText1:
      "Аби додати симптоми, оберіть необхідний день та занотуйте їх.",
    personalSText: (
      <div>
        <ul style={{ color: "green" }}>
          <li>
            Ніякого кашлю, хрипів, напруги у грудях протягом дня або ночі.
          </li>
          <li>Можу робити звичайні заняття.</li>
        </ul>
        <ul style={{ color: "orange" }}>
          <li>Кашель.</li>
          <li>Хрип.</li>
          <li>Тісність грудної клітки.</li>
          <li>Задишка.</li>
          <li>Прокидаючись вночі через астму.</li>
          <li>
            Можу займатися деякими, але не всіма звичайними видами діяльності
          </li>
        </ul>
        <ul style={{ color: "red" }}>
          <li>Сильна задишка. </li>
          <li>Ліки з швидким полегшенням не допомогають. </li>
          <li>Неможливо робити звичайні дії</li>
        </ul>
      </div>
    ),
    personalT: "Тест",
    personalG: "Графік",
    tesTask1:
      "Як часто за останній тиждень астма заважала тобі виконувати звичне навантаження?",
    tesTask2: "Як часто за останній тиждень ви помічали утруднене дихання?",
    tesTask3:
      "Як часто за останній тиждень ви прокидалися вночі або раніше, ніж зазвичай, через симптоми астми?",
    tesTask4: "Як часто ви вживали ліки за останній тиждень?",
    tesTask5:
      "Як би ви оцінили, як вам вдалося контролювати симптоми астми за останній тиждень?",
    testAnsw11: "Весь час або дуже часто",
    testAnsw12: "Іноді",
    testAnsw13: "Рідко або ніколи",
    testAnsw21: "Один раз на день або частіше",
    testAnsw22: "3-6 рази на тиждень",
    testAnsw23: "1-2 рази на тиждень або ніколи",
    testAnsw31: "3-4 ночі на тиждень або частіше",
    testAnsw32: "1-2 ночі на тиждень",
    testAnsw33: "1 ніч на тиждень або ніколи",
    testAnsw41: "3 рази на тиждень або частіше",
    testAnsw42: "1-2 рази на тиждень",
    testAnsw43: "1 рази на тиждень або ніколи",
    testAnsw51: "Погано вдавалося",
    testAnsw52: "Вдавалося за деяких виключень",
    testAnsw53: "Добре вдавалося"
  }
};
