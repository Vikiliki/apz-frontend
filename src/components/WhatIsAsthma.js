import React from "react";
import styled from "styled-components";
import { Grid } from "@material-ui/core";
import asthma from "../images/asthma.jpg";
import { localizedText } from "./Localization";


const Text = styled.p`
  text-align: justify;
`

export function WhatIsAsthma({ language }) {
  return (
    <div>
        {localizedText[language].whatAsthmaTitle}
      <Grid container spasing={2}>
        <Grid item xs={1}>
        </Grid>
        <Grid item xs={5}>
          <Text>
          {localizedText[language].whatAsthmaText}
          </Text>
        </Grid>
        <Grid item xs={1}></Grid>
        <Grid item xs={3}>
          <img alt={"img"} src={asthma} style={{width: '100%'}} />
        </Grid>
      </Grid>
{localizedText[language].typeTitle}
      <Grid container spasing={2}>
      <Grid item xs={1}></Grid>
      <Grid item xs={10}>
      <Text>
      {localizedText[language].typeText}
        </Text>
        </Grid>
        <Grid item xs={1}></Grid>
        </Grid>
    </div>
  );
}
