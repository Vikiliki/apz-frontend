import React from "react";
import useChartConfig from "hooks/useChartConfig";
import Box from "@material-ui/core/Box";
import SyntaxHighlighter from "react-syntax-highlighter";
import { Chart } from "react-charts";
let sourceCode;
export function Graph() {
  const { data, randomizeData } = useChartConfig({
    series: 10
  });
  const series = React.useMemo(
    () => ({
      showPoints: false
    }),
    []
  );
  const axes = React.useMemo(
    () => [
      { primary: true, type: "time", position: "bottom" },
      { type: "linear", position: "left" }
    ],
    []
  );
  return (
    <>
      <button onClick={randomizeData}>Randomize Data</button>
      <br />
      <br />
      <Box>
        <Chart data={data} series={series} axes={axes} tooltip />
      </Box>
      <br />
      <SyntaxHighlighter code={sourceCode} />
    </>
  );
}

