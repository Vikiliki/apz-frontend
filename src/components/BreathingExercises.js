import React from "react";
import styled from "styled-components";
import { Grid } from "@material-ui/core";
import breath1 from "../images/breath1.png";
import breath2 from "../images/breath2.png";
import breath3 from "../images/breath3.png";
import { localizedText } from "./Localization";

const Text = styled.p`
  text-align: justify;
`;

export function BreathingExercises({ language }) {
  return (
    <div>
      {localizedText[language].breathTitle}
      <Grid container spasing={3}>
        <Grid item xs={1}></Grid>
        <Grid item xs={5}>
          <Text>{localizedText[language].breathText}</Text>
        </Grid>
        <Grid item xs={1}></Grid>
        <Grid item xs={3}>
          <img alt={"img"} src={breath3} style={{ width: "100%" }} />
          <img alt={"img"} src={breath1} style={{ width: "100%" }} />
          <img alt={"img"} src={breath2} style={{ width: "100%" }} />
        </Grid>
      </Grid>
    </div>
  );
}
