import axios from "axios";

function getAuthHeaderValue() {
  const tokenType = window.localStorage.getItem("tokenType");
  const token = window.localStorage.getItem("token");
  return `${tokenType} ${token}`;
}

export function isAuthenticated() {
  const token = window.localStorage.getItem("token");
  return Boolean(token);
}

export async function signUp(values) {
  try {
    const response = await axios.post(
      window.settings.ENDPOINTS.SIGN_UP_URL,
      values
    );
    window.localStorage.setItem("token", response.data.token);
    window.localStorage.setItem("tokenType", "Token");
    window.location.reload();
  } catch (error) {
    console.log(error);
  }
}

export async function signIn(values) {
  try {
    const response = await axios.post(
      window.settings.ENDPOINTS.SIGN_IN_URL,
      values
    );
    window.localStorage.setItem("token", response.data.token);
    window.localStorage.setItem("tokenType", "Token");
    window.location.reload();
  } catch (error) {
    console.log(error);
  }
}

export async function getWithAuth(url, headers = {}) {
  return await axios.get(url, {
    headers: {
      Authorization: getAuthHeaderValue(),
      ...headers
    }
  });
}

export async function postWithAuth(url, data = {}, headers = {}) {
  return await axios.post(url, data, {
    headers: { ...headers, Authorization: getAuthHeaderValue() }
  });
}

export async function patchWithAuth(url, data = {}, headers = {}) {
  return await axios.patch(url, data, {
    headers: {
      ...headers,
      Authorization: getAuthHeaderValue()
    }
  });
}

export async function deleteWithAuth(url, headers = {}) {
  return await axios.delete(url, {
    headers: { ...headers, Authorization: getAuthHeaderValue() }
  });
}
