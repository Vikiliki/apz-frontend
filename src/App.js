import React, { useState } from "react";
import "./App.css";
import { Tabs, Tab } from "@material-ui/core";
import { Home } from "./components/Home";
import { TabPanel } from "./components/TabPanel";
import { WhatIsAsthma } from "./components/WhatIsAsthma";
import { Symptoms } from "./components/Symptoms";
import { Prevention } from "./components/Prevention";
import { BreathingExercises } from "./components/BreathingExercises";
import { AsthmaTriggers } from "./components/AsthmaTriggers";
import { Personal } from "./components/Personal";
import { Test } from "./components/Test";
import { SignIn } from "./components/SignIn";
import { SignUp } from "./components/SignUp";
import styled from "styled-components";
import "./settings";
import { isAuthenticated } from "./api";

const TabsStyleWrapper = styled.div`
  background-color: white;
`;

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    "aria-controls": `scrollable-auto-tabpanel-${index}`
  };
}

function App() {
  const [currentTabHook, setCurrentTabHook] = useState(0);
  const [language, setLanguage] = useState("en");
  return (
    <div className="App">
      <TabsStyleWrapper>
        <Tabs
          value={currentTabHook}
          onChange={(e, newValue) => setCurrentTabHook(newValue)}
          indicatorColor="primary"
          textColor="primary"
          centered
          variant="scrollable"
          scrollButtons="auto"
        >
          <Tab label="Home" {...a11yProps(0)} />
          <Tab label="What's asthma" {...a11yProps(1)} />
          <Tab label="Symptoms" {...a11yProps(2)} />
          <Tab label="Asthma Triggers" {...a11yProps(3)} />
          <Tab label="Prevention" {...a11yProps(4)} />
          <Tab label="Breathing Exercises" {...a11yProps(5)} />
          <Tab
            label="Personal"
            {...a11yProps(6)}
            style={{ display: isAuthenticated() ? "block" : "none" }}
          />
          <Tab label="Test" {...a11yProps(7)} style={{ display: "none" }} />
          <Tab label="SignIn" {...a11yProps(8)} style={{ display: "none" }} />
          <Tab label="SignUp" {...a11yProps(9)} style={{ display: "none" }} />
        </Tabs>
      </TabsStyleWrapper>
      <TabPanel value={currentTabHook} index={0}>
        <Home
          currentTabSetter={setCurrentTabHook}
          signInIndex={8}
          signUpIndex={9}
          language={language}
          setLanguage={setLanguage}
        />
      </TabPanel>
      <TabPanel value={currentTabHook} index={1}>
        <WhatIsAsthma language={language} />
      </TabPanel>
      <TabPanel value={currentTabHook} index={2}>
        <Symptoms language={language} />
      </TabPanel>
      <TabPanel value={currentTabHook} index={3}>
        <AsthmaTriggers language={language} />
      </TabPanel>
      <TabPanel value={currentTabHook} index={4}>
        <Prevention language={language} />
      </TabPanel>
      <TabPanel value={currentTabHook} index={5}>
        <BreathingExercises language={language} />
      </TabPanel>
      <TabPanel value={currentTabHook} index={6}>
        <Personal
          language={language}
          currentTabSetter={setCurrentTabHook}
          testPageIndex={7}
        />
      </TabPanel>
      <TabPanel value={currentTabHook} index={7}>
        <Test language={language} />
      </TabPanel>
      <TabPanel value={currentTabHook} index={8}>
        <SignIn language={language} />
      </TabPanel>
      <TabPanel value={currentTabHook} index={9}>
        <SignUp language={language} />
      </TabPanel>
    </div>
  );
}

export default App;
